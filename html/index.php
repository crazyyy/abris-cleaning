<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> </title>

    <link href="http://www.google-analytics.com/" rel="dns-prefetch"><!-- dns prefetch -->
    <!-- meta -->

    <!-- icons -->
    <link href="favicon.ico" rel="shortcut icon">

    <!-- css + javascript -->
    <link rel="stylesheet" href="style.css" media="all">
    <script type="text/javascript" src="///cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
</head>
<body>
<!-- wrapper -->
<div class="wrapper">
  <header role="banner">
    <div class="inner">

      <div class="logo">
        <img src="img/logo.png" alt="" title="">
      </div><!-- /logo -->

      <nav class="nav" role="navigation">
        <ul>
          <li class="menuItem current"><a href="#home">Главная</a></li>
          <li class="menuItem"><a href="#features">Услуги</a></li>
          <li class="menuItem"><a href="#about">О нас</a></li>
          <li class="menuItem"><a href="#supers">Преимущества</a></li>
          <li class="menuItem"><a href="#review">Отзывы</a></li>
          <li class="menuItem"><a href="#contact">Контакты</a></li>
        </ul>
      </nav><!-- /nav -->

      <ul class="phone-block">
        <li><a href="tel:83838888888">8 (383) 888 88 88</a></li>
        <li><a href="tel:89231445240">8 (923) 144 52 40</a></li>
        <li><span class="recall">заказать звонок</span></li>
      </ul>

    </div><!-- /.inner -->
  </header><!-- /header -->

  <section role="main">

    <article id="home" class="block-first">
      <div class="inner">
        <div class="block-first-heading">
          <span>Ваш</span><span>дом, коттедж, офис,<br>квартира станут чистыми </span><span>уже через 4 часа</span>
        </div><!-- /.block-first-heading -->
        <div class="block-first-content">руками обученных<br>сотрудников, которые<br>уже убрали <span class="white">9000 домов</span><br><span class="violet">в Новосибирске и НСО</span></div>
        <div class="order-form">
          <h5>скидка 15%</h5>
          <h6>на все услуги компании</h6>
          <form action="data/form.php" method="post">
            <div class="timer"></div>
            <input type="text" name="name" placeholder="Имя">
            <input type="text" name="phone" placeholder="Телефон">
            <input type="hidden" name="email" value="email@abris-cleaning.info">
            <input type="hidden" name="hidden" value="ok">
            <button class="btn btn-orange">получить скидку</button>
            <h4 class="message">Отправляем...</h4>
            <div class="thanx">
              <h4>Благодарим вас за заявку <br><span>Менеджер свяжется с вами в ближайшее время.</span></h4>
            </div><!-- thanx -->
          </form>
        </div>
      </div><!-- /.inner -->
    </article><!-- /.block-first -->

    <article class="block-second scroll-animate animate">
      <ul class="inner">
        <li><span class="num" data-number="10">10</span>лет на рынке профессиональной уборки</li>
        <li><span class="num" data-number="25">25</span>мобильных бригад</li>
        <li><span class="num" data-number="1000">1000</span>постоянных клиентов</li>
      </ul><!-- /.inner -->
    </article><!-- /.block-second -->

    <article id="features" class="block-third">
      <div class="inner">
        <h1><span>САМЫЕ</span> ПОПУЛЯРНЫЕ УСЛУГИ</h1>
        <h2>Все виды услуг оказываются круглосуточно и без выходных</h2>

        <ul class="services-list">
          <li href="" class="service-link s1" data-service="s1">
            Уборка <span>комплексная</span>
          </li>
          <li href="" class="service-link s2" data-service="s2">
            Уборка <span>генеральная</span>
          </li>
          <li href="" class="service-link s3" data-service="s3">
            Уборка <span>после ремонта</span>
          </li>
          <li href="" class="service-link s4" data-service="s4">
            Мытье <span>фасадов, окон, витрин</span>
          </li>
          <li href="" class="service-link s5" data-service="s5">
            Для <span>юридических лиц</span>
          </li>
          <li href="" class="service-link s6" data-service="s6">
            <span>Доп. услуги</span>
          </li>

        </ul><!-- services-list -->

        <div class="service-block service-block-s2" id="s1">
          <p>В поддерживающею уборку квартиры или коттеджа входит уборка и мойка всех поверхностей не выше 2-х метров от пола. <span>ПОЛНЫЙ ПЕРЕЧЕНЬ УСЛУГ ВКЛЮЧАЕТ В СЕБЯ:</span></p>
          <ul>
            <li>Уборку комнат и коридоров – пол, плинтус, зеркало, суха чистка ковров, мебель снаружи, вынос мусора.</li>
            <li>Уборку ванной комнаты и туалета – пол, мебель, ванна/душ, туалет, раковина, сантехника, стиральная машина, стены, мебель внутри и снаружи</li>
            <li>Уборку на кухне – пол, мебель, посуда, микроволновка/плита/холодильник снаружи, вытяжка, удаление местных загрязнений, вынос мусора</li>
          </ul>
          <h3>от 1 800 руб.</h3>
          <a href="#" class="btn btn-orange">от 1 800 руб.</a>
          <a href="#" class="price-dl">скачать полный прайс-лист</a>
        </div><!-- service-block -->

        <div class="service-block service-block-s2" id="s2">
          <p>В генеральную уборку квартиры или коттеджа входит уборка и мойка всех поверхностей. <span>ПОЛНЫЙ ПЕРЕЧЕНЬ УСЛУГ ВКЛЮЧАЕТ В СЕБЯ:</span></p>
          <ul>
            <li>Уборку комнат и коридоров – пол, стены, мебель внутри и снаружи, антресоли, бытовая техника, сухая чистка ковров, карнизы, двери, батареи, розетки, люстры, все труднодоступные места</li>
            <li>Уборку ванной комнаты и туалета – пол, стены, мебель внутри и снаружи, ванна/душ, туалет, раковина, сантехника, стиральная машина</li>
            <li>Уборку на кухне – пол, мебель, посуда, микроволновка/плита/холодильник снаружи, вытяжка, удаление местных загрязнений, вынос мусора</li>
          </ul>
          <h3>от 1 800 руб.</h3>
          <a href="#" class="btn btn-orange">от 1 800 руб.</a>
          <a href="#" class="price-dl">скачать полный прайс-лист</a>
        </div><!-- service-block -->

        <div class="service-block service-block-s2" id="s3">
          <p>Уборка после ремонта включает в себя комплексную чистку и мойку всех поверхностей. <span>ПОЛНЫЙ ПЕРЕЧЕНЬ УСЛУГ ВКЛЮЧАЕТ В СЕБЯ:</span></p>
          <ul>
            <li>Уборку комнат и коридоров – пол, стены, мебель внутри и снаружи, антресоли, бытовая техника, сухая чистка ковров, карнизы, двери, батареи, розетки, люстры, все труднодоступные места</li>
            <li>Уборку ванной комнаты и туалета – пол, стены, мебель внутри и снаружи, ванна/душ, туалет, раковина, сантехника, стиральная машина</li>
            <li>Уборку на кухне – пол, мебель, посуда, микроволновка/плита/холодильник снаружи, вытяжка, удаление местных загрязнений, вынос мусора</li>
          </ul>
          <h3>от 1 800 руб.</h3>
          <a href="#" class="btn btn-orange">от 1 800 руб.</a>
          <a href="#" class="price-dl">скачать полный прайс-лист</a>
        </div><!-- service-block -->

        <div class="service-block service-block-s2" id="s4">
          <p>Мойка окон в квартире или коттедже включает в себя ряд особенностей. <span>ПОЛНЫЙ ПЕРЕЧЕНЬ УСЛУГ ВКЛЮЧАЕТ В СЕБЯ:</span></p>
          <ul>
            <li>Мойку окон – рамы, стеклянные поверхности, москитные сетки. </li>
            <li>Около оконное пространство – подоконники, отопительные батареи, карнизы</li>
            <li>Придание блеска всем частям окна</li>
          </ul>
          <h3>от 1 800 руб.</h3>
          <a href="#" class="btn btn-orange">от 1 800 руб.</a>
          <a href="#" class="price-dl">скачать полный прайс-лист</a>
        </div><!-- service-block -->

        <div class="service-block service-block-s2" id="s5">
          <p>В поддерживающею уборку квартиры или коттеджа входит уборка и мойка всех поверхностей не выше 2-х метров от пола. <span>ПОЛНЫЙ ПЕРЕЧЕНЬ УСЛУГ ВКЛЮЧАЕТ В СЕБЯ:</span></p>
          <ul>
            <li>Уборку комнат и коридоров – пол, плинтус, зеркало, суха чистка ковров, мебель снаружи, вынос мусора.</li>
            <li>Уборку ванной комнаты и туалета – пол, мебель, ванна/душ, туалет, раковина, сантехника, стиральная машина, стены, мебель внутри и снаружи</li>
            <li>Уборку на кухне – пол, мебель, посуда, микроволновка/плита/холодильник снаружи, вытяжка, удаление местных загрязнений, вынос мусора</li>
          </ul>
          <h3>от 1 800 руб.</h3>
          <a href="#" class="btn btn-orange">от 1 800 руб.</a>
          <a href="#" class="price-dl">скачать полный прайс-лист</a>
        </div><!-- service-block -->

        <div class="service-block service-block-s2" id="s6">
          <p>В поддерживающею уборку квартиры или коттеджа входит уборка и мойка всех поверхностей не выше 2-х метров от пола. <span>ПОЛНЫЙ ПЕРЕЧЕНЬ УСЛУГ ВКЛЮЧАЕТ В СЕБЯ:</span></p>
          <ul>
            <li>Уборку комнат и коридоров – пол, плинтус, зеркало, суха чистка ковров, мебель снаружи, вынос мусора.</li>
            <li>Уборку ванной комнаты и туалета – пол, мебель, ванна/душ, туалет, раковина, сантехника, стиральная машина, стены, мебель внутри и снаружи</li>
            <li>Уборку на кухне – пол, мебель, посуда, микроволновка/плита/холодильник снаружи, вытяжка, удаление местных загрязнений, вынос мусора</li>
          </ul>
          <h3>от 1 800 руб.</h3>
          <a href="#" class="btn btn-orange">от 1 800 руб.</a>
          <a href="#" class="price-dl">скачать полный прайс-лист</a>
        </div><!-- service-block -->

      </div><!-- /.inner -->
    </article><!-- /.block-third -->

    <article id="about" class="block-fourth">
      <div class="inner">
        <h1>КАК МЫ <span>НАВОДИМ ЧИСТОТУ</span> В ДОМЕ</h1>
        <div class="yotuber">
          <iframe width="813" height="610" src="https://www.youtube.com/embed/6OIt_hPhgSw?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
        </div><!-- /.yotuber -->
        <a href="" class="btn btn-orange recall">Заказать уборку</a>
      </div><!-- /.inner -->
    </article><!-- /.block-fourth -->

    <article class="block-six">
      <div class="inner">
        <h1><span>5 ШАГОВ</span> К ЧИСТОТЕ И УЮТУ У ВАС ДОМА</h1>
        <ul>
          <li><span>шаг 1</span>Вы оставляете заявку на сайте<a href="#" class="btn btn-small btn-orange recall">Оставить заявку</a></li>
          <li><span>шаг 2</span>Наш специалист связывается с вами и консультирует по всем вопросам</li>
          <li><span>шаг 3</span>Мы рассчитываем стоимость</li>
          <li><span>шаг 4</span>Мы выезжаем и проводим уборку</li>
          <li><span>шаг 5</span>Вы оплачиваете только после оказания услуг</li>
        </ul>
      </div><!-- /.inner -->
    </article><!-- /.block-six -->

    <article id="supers" class="block-seven">
      <div class="inner">

        <h3><span>почувствуйте</span> разницу сами</h3>
        <div class="block-left">
          <h4>С чем можно столкнуться при обращении к непрофессионалам</h4>
          <ul>
            <li>Опоздание или срыв заказа</li>
            <li>Не соблюдение сроков оказания услуг</li>
            <li>Трудно связаться с менеджером</li>
            <li>Не квалифицированный персонал</li>
            <li>Стандартные тарифы на услуги</li>
            <li>Отсутствие гарантий</li>
          </ul>
        </div><!-- block-left -->
        <div class="block-right">
          <h4>Обращение к профессионалам <span>Абрис-Клининг</span></h4>
          <ul>
            <li>Прибытие точно в срок</li>
            <li>Своевременное выполнение заказа</li>
            <li>Круглосуточная работа Call-центра</li>
            <li>Только обученные сотрудники</li>
            <li>Гибкая ценовая политика, акции и спецпредложения</li>
            <li>Оплата только после оказания услуг</li>
          </ul>
        </div><!-- /.block-right -->

      </div><!-- /.inner -->
    </article><!-- /.block-seven -->

    <article class="block-eight">
      <div class="inner">

        <h1><span>Результаты</span> нашей работы</h1>

        <div class="owl-carousel">
          <div><img src="img/results.jpg" alt=""></div>
          <div><img src="img/results.jpg" alt=""></div>
          <div><img src="img/results.jpg" alt=""></div>
        </div>

        <a href="#" class="btn btn-orange recall">Хочу так же</a>

      </div><!-- /.inner -->
    </article><!-- /.block-eight -->

    <article class="block-nine">
      <div class="inner">

        <h1>ПОЧЕМУ <span>ВЫБИРАЮТ</span><br> ИМЕННО <span>НАС</span></h1>
        <ul>
          <li>
            <h4><span>ТОЛЬКО ОБУЧЕННЫЕ</span> СОТРУДНИКИ</h4>
            <p>Весь персонал перед выездом на объект проходит подробный инструктаж, чтобы качество уборки оставалось на высоком уровне и были учтены особенности Вашего объекта. За Вами закрепляется персональный менеджер.</p>
          </li>
          <li>
            <h4><span>круглосуточная </span>мобильная бригада</h4>
          </li>
          <li>
            <h4>срочный выезд <span>на уборку</span></h4>
          </li>
          <li>
            <h4><span>современное</span> оборудование</h4>
          </li>
        </ul>

        <div class="box-blue">
          <span>>90%</span>
          <p>клиентов воспользовавшись нашими услугами, обращаются к нам повторно и становятся постоянными клиентами Абрис-Клининг</p>
        </div><!-- /.box-blue -->

        <div class="box-transparent">
          <p>Мы несем полную материальную ответственность за обслуживаемый нами объект. Если в ходе нашей работы будет что-то испорчено, то мы незамедлительно возместим Вам ущерб.</p>
        </div><!-- /.box-transparent -->

      </div><!-- /.inner -->
    </article><!-- /.block-nine -->

    <article class="block-eleven">
      <div class="inner">

        <h1>НУЖНО <span>СРОЧНО УБРАТЬ</span><br> ДОМ ИЛИ КОТТЕДЖ?</h1>
        <ul>
          <li><span>> 12 000</span>домов убрано</li>
          <li><span>> 200</span>сотрудников в компании</li>
        </ul>

        <form action="data/form.php" method="post">
          <p>После отправки заказа наши специалисты свяжутся с Вами ближайшее время</p>
          <input type="text" name="name" placeholder="Имя">
          <input type="text" name="phone" placeholder="Телефон">
          <input type="hidden" name="email" value="email@abris-cleaning.info">
          <input type="hidden" name="hidden" value="ok">
          <button class="btn btn-orange">заказать уборку</button>
          <h4 class="message">Отправляем...</h4>
          <div class="thanx">
            <h4>Благодарим вас за заявку <br><span>Менеджер свяжется с вами в ближайшее время.</span></h4>
          </div><!-- thanx -->
        </form>

      </div><!-- /.inner -->
    </article><!-- /.block-eleven -->

    <article id="review" class="block-twelve">
      <div class="inner">

        <h1>отзывы <span>о нашей работе</span></h1>

        <div class="review-carousel">
          <div>
            <img src="img/reviews.jpg" alt="">
            <h4 class="vk"><a href="http://vk.com/allex8711">Alexander Bogatyrev</a></h4>
            <p>Заказал уборку коттеджа после ремонта и мойку окон. Персонал в фирме опытный, это чувствуется. Работали четко, разделили обязанности, пользовались специальным оборудованием, которое привезли с собой. Качеством услуг доволен, рекомендую. Заказал уборку коттеджа после ремонта и мойку окон. Персонал в фирме опытный, это чувствуется.</p>
          </div>
          <div>
            <img src="img/reviews1.jpg" alt="">
            <h4 class="vk"><a href="https://vk.com/id29394840">Olga Vasenyova</a></h4>
            <p>Заказывала генеральную уборку трехкомнатной квартиры плюс мойку окон. Отличная работа, придраться не к чему. Приехали две девушки, сделали все за 4 часа. Приветливые, вежливые, опрятные. Уложились в сроки. Спасибо за отличную работу.</p>
          </div>
          <div>
            <img src="img/reviews2.jpg" alt="">
            <h4 class="vk"><a href="http://vk.com/maximnazarenko">Maxim Nazarenko</a></h4>
            <p>Я думал, что мойка окон входит в поддерживающую уборку. Оказалось, ее нужно дополнительно заказывать. Правда, вежливая барышня на телефоне подсказала, что есть скидка. Спасибо. Буду привыкать к новым стандартам чистоты – с вашей компанией. Порадовало качество, сервис, улыбчивость девушки-уборщицы.</p>
          </div>
          <div>
            <img src="img/reviews3.jpg" alt="">
            <h4 class="vk"><a href="http://vk.com/smirnova_elena97">Alyona Smirnova</a></h4>
            <p>Заказала чистку кухни, ванны и туалета. Заявку давала на сайте – удобно и быстро. Менеджер перезвонила тут же. Порадовала опрятность и доброжелательность уборщицы — улыбчивая, немногословная, в форме с логотипом. Сделала все на отлично, спасибо большое.</p>
          </div>
        </div><!-- review-carousel -->

      </div><!-- /.inner -->
    </article><!-- /.block-twelve -->

    <article id="contact" class="block-thirteen">
      <div class="box-map">

      </div><!-- /.box-map -->
      <div class="inner">
        <div class="box-contact">
          <h5>Контакты</h5>
          <p>г.Новосбирск, ул.Челюскинцев 44/2</p>
          <a href="tel:83838888888">8 (383) 888 88 88</a>
          <a class="phone2" href="tel:89231445240">8 (923) 144 52 40</a>
          <h6>закажите уборку</h6>
          <form action="data/form.php" method="post">
            <p>После отправки заказа наши специалисты свяжутся с Вами ближайшее время</p>
            <input type="text" name="name" placeholder="Имя">
            <input type="text" name="phone" placeholder="Телефон">
            <input type="hidden" name="email" value="email@abris-cleaning.info">
            <input type="hidden" name="hidden" value="ok">
            <button class="btn btn-orange">заказать уборку</button>
            <h4 class="message">Отправляем...</h4>
            <div class="thanx">
              <h4>Благодарим вас за заявку <br><span>Менеджер свяжется с вами в ближайшее время.</span></h4>
            </div><!-- thanx -->
            </form>
        </div><!-- /.box-contact -->

      </div><!-- /.inner -->
    </article><!-- /.block-thirteen -->

    </div><!-- /.inner -->
  </section><!-- /section -->
</div><!-- /wrapper -->

<footer role="contentinfo">
  <div class="inner">

    <p class="copyright">© 2004-2015 OOO" Абрис" ОГРН 1145476010877 <br>Все права защищены <br> <a href="">Политика конфиденциальности</a></p><!-- /copyright -->

  </div><!-- /.inner -->
</footer><!-- /footer -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery.js"><\/script>')</script>
    <script type="text/javascript" src="js/stickUp.min.js"></script>
    <script type="text/javascript" src="js/jquery.maskedinput.min.js"></script>
    <script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="js/jquery.plugin.min.js"></script>
    <script type="text/javascript" src="js/jquery.countdown.min.js"></script>
    <script type="text/javascript" src="js/jquery.countdown-ru.js"></script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script type="text/javascript" src="js/modal.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>

</body>
</html>
